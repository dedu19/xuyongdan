package com.example.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class Person implements Serializable {
    private static final long serialVersionID = 2895782870082326368L;
    private int id;
    private String name;
    private String salary;
    private String deptid;
}
