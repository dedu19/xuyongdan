package com.example.Dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FindData {
    void findDataExport(@Param("list") List<String> list);
}
