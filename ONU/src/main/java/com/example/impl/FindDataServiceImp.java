package com.example.impl;

import com.example.Dao.FindData;
import com.example.service.FindDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.jta.WebSphereUowTransactionManager;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

@Service
public class FindDataServiceImp implements FindDataService {
    @Autowired
    private FindData findData;
    @Override
    public void findDataTest(List<String> list) {
        findData.findDataExport(list);
    }

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("正在阻塞");
        Socket socket = serverSocket.accept();
        InputStream inputStream = socket.getInputStream();
        byte[] bytes = new byte[10];
        int readLine = inputStream.read(bytes);
        while (readLine != -1) {
            for (int i = 0; i < readLine; i++) {
                System.out.print(bytes[i]);
            }
        }
        socket.close();
        inputStream.close();
        System.out.println("结束了");
    }



}
