package com.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/hello")
@RestController
@Slf4j
public class creatDan {
    @RequestMapping(value = "/{name}")
    public String hello(@PathVariable String name) {
        log.info("{} 你好",name);
        log.trace("trace");
        log.debug("debug");
        log.warn("warn");
        log.info("info");
        log.error("error");
        return "Hello World "+name;
    }

}
class threadDan implements Runnable{

    @Override
    public void run() {

    }
}
