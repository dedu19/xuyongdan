package com.example.controller;


import org.apache.logging.log4j.util.Strings;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.FileInputStream;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ExcelTest {

    public static void main(String[] args) {
        ArrayList<Person> objects = new ArrayList<>();
        Person yantian = new Person("yantian", 19);
        Person yongdan = new Person("yongdan", 18);
        objects.add(yantian);
        objects.add(yongdan);
        System.out.println(objects);
//        try {
//            // excel文件路径，自行修改成自己的
//            String excelFileName = "D:/Desktop/xyd.xlsx";
//            // 第2个boolean参数为是否跳过第1行表头的参数，如果需要跳过表可，可传true
//            List<List<String>> lists = readExcle(excelFileName, true);
//
//            StringBuffer rowContent = new StringBuffer();
//            AtomicInteger row = new AtomicInteger(1);
//            lists.forEach(list -> {
//                rowContent.append("第" + row + "行:");
//                StringBuffer columnContent = new StringBuffer();
//
//                list.forEach(value -> {
//                    columnContent.append(",").append(value);
//                });
//
//                rowContent.append(columnContent.toString().
//                        substring(1)).append("\r\n");
//                row.getAndIncrement();
//            });
//
//            System.out.println(rowContent.toString());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }


    public static List<List<String>> readExcle(String fileName, boolean skipFirstRow) throws Exception {

        //new一个输入流
        FileInputStream inputStream = new FileInputStream(fileName);
        //new一个workbook
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

        //创建一个sheet对象，参数为sheet的索引
        XSSFSheet sheet = workbook.getSheetAt(0);
        //new出存放一张表的二维数组
        List<List<String>> allData = new ArrayList<List<String>>();

        for (Row row : sheet) {
            List<String> oneRow = new ArrayList<String>();
            //不读表头
            if (skipFirstRow && row.getRowNum() == 0)
                continue;
            for (Cell cell : row) {
                cell.setCellType(CellType.STRING);
                oneRow.add(cell.getStringCellValue().trim());
            }
            allData.add(oneRow);
        }

        //关闭workbook
        workbook.close();
        return allData;
    }
}

class Person{
    String name;
    int age;
    public Person(){};
    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}