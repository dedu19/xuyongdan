import java.util.Arrays;
import java.util.HashMap;

public class Test3 {

    public static int[] twoSum(int[] nums, int target) {

        HashMap<Integer, Integer> objectHashMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (objectHashMap.containsKey(nums[i])) {
                Integer n = objectHashMap.get(nums[i]);
                return new int[]{n,i};
            } else {
                objectHashMap.put(target - nums[i],i);
            }
        }
        return null;

    }

    public static void main(String[] args) {
        int[] ints = {2, 3, 4, 5, 6, 7};
        int[] ints1 = twoSum(ints, 7);
        System.out.println(Arrays.toString(ints1));
    }

}
